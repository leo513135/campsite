const mongoose = require('mongoose')
const cities = require('./cities')
const { places, descriptors } = require('./seedHelpers')
const Campground = require('../models/campground')
main().catch(err => console.log(err));

async function main() {
    await mongoose.connect('mongodb://127.0.0.1/yelp-camp')
        .then(() => {
            console.log('Connection Start')
        }).catch(err => {
            console.log(err)
            console.log('Oh No Error!~!!!')
        })
}

const sample = array => array[Math.floor(Math.random() * array.length)]

const seedDB = async () => {
    await Campground.deleteMany({})
    for (let i = 0; i < 50; i++) {
        const random1000 = Math.floor(Math.random() * 1000)
        const price = Math.floor(Math.random() * 20) + 10
        const camp = new Campground({
            author: '641c5b9e41da3a2bc327b6f6',
            location: `${cities[random1000].city}, ${cities[random1000].state}`,
            title: `${sample(descriptors)} ${sample(places)}`,
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias voluptatem nobis sapiente molestiae libero labore assumenda laborum quibusdam minus laboriosam perferendis dolor omnis, distinctio, veniam totam non. Sunt, in minima?',
            price,
            images: [
                {
                    url: 'https://res.cloudinary.com/dnpzedhdw/image/upload/v1680272695/YelpCamp/ddq9e7sfsato1uhj7kiu.jpg',
                    filename: 'YelpCamp/ddq9e7sfsato1uhj7kiu'
                }
            ]
        })
        await camp.save()
    }
}

seedDB().then(() => {
    mongoose.connection.close()
})